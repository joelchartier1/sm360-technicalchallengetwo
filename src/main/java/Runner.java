import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Runner {

    private long startTime;
    private double maxProfit = -1.0;
    private ActionPoint buyInActionPoint = null;
    private ActionPoint buyOutActionPoint = null;

    public void run(final String filename) {

        startTime = System.currentTimeMillis();
        maxProfit = -1.0;

        final List<ActionPoint> history = extractHistory(filename);
        final List<ActionPoint> clonedHistory = getOrderedHistory(history);

        final ActionPoint smallestValue = clonedHistory.get(0);
        final ActionPoint highestValue = clonedHistory.get(clonedHistory.size() - 1);

        if (historyIsNicelyOrdered(smallestValue, highestValue)) {

            setFoundResults(smallestValue, highestValue);
        } else {

            findResultByIteration(history);
        }

        printResult();
    }

    private void setFoundResults(final ActionPoint smallestValue, final ActionPoint highestValue) {

        maxProfit = highestValue.getValue() - smallestValue.getValue();
        buyInActionPoint = smallestValue;
        buyOutActionPoint = highestValue;
    }

    private boolean historyIsNicelyOrdered(final ActionPoint smallestValue, final ActionPoint highestValue) {

        return smallestValue.getTime().compareTo(highestValue.getTime()) < 0;
    }

    private void findResultByIteration(final List<ActionPoint> history) {

        final int historySize = history.size();

        for (int i = 0; i < historySize; i++) {

            for (int j = (i+1); j < historySize; j++) {

                final ActionPoint currentActionPoint = history.get(i);
                final ActionPoint comparedActionPoint = history.get(j);

                final double possibleProfit = comparedActionPoint.getValue() - currentActionPoint.getValue();

                if (maxProfit <= possibleProfit) {

                    maxProfit = possibleProfit;
                    buyInActionPoint = currentActionPoint;
                    buyOutActionPoint = comparedActionPoint;
                }
            }
        }
    }

    private void printResult() {

        System.out.println(String.format("Maximal profit: [%f]", maxProfit));
        System.out.println(String.format("Buy in action: [%s] @ [%s]", buyInActionPoint.getValue(), buyInActionPoint.getTime()));
        System.out.println(String.format("Buy out action: [%s] @ [%s]", buyOutActionPoint.getValue(), buyOutActionPoint.getTime()));
        System.out.println(String.format("Execution time: %d ms", System.currentTimeMillis() - startTime));
    }

    private List<ActionPoint> getOrderedHistory(final List<ActionPoint> history) {

        final List<ActionPoint> clonedHistory = new ArrayList<>(history);

        Collections.sort(clonedHistory, new Comparator<ActionPoint>() {
            @Override
            public int compare(ActionPoint o1, ActionPoint o2) {

                return o1.getValue().compareTo(o2.getValue());
            }
        });

        return clonedHistory;
    }

    private List<ActionPoint> extractHistory(final String arg) {

        final List<ActionPoint> entries = new ArrayList<>();

        try {

            final BufferedReader dataBR = new BufferedReader(new FileReader(new File(arg)));
            String line;

            // ignoring header
            dataBR.readLine();

            while ((line = dataBR.readLine()) != null) {

                final String[] entry = line.split(",", 2);
                entries.add(new ActionPoint(entry[0], Double.valueOf(entry[1])));
            }
        } catch (Exception ignored) {}

        return entries;
    }
}