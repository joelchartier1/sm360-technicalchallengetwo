public class ActionPoint {

    private String time;
    private Double value;

    public ActionPoint() {}

    public ActionPoint(final String time, final Double value) {

        this.time = time;
        this.value = value;
    }

    public String getTime() {

        return time;
    }

    public Double getValue() {

        return value;
    }

}